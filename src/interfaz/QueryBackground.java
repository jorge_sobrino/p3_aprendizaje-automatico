package interfaz;

import javax.swing.*;
import java.awt.*;

public class QueryBackground extends JPanel{

    public QueryBackground(){
        this.setSize(500,400);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        ImageIcon fondologin = new ImageIcon("images/rafaNadal.jpg");
        g.drawImage(fondologin.getImage(), 0,0,1200, 715, null);
        setOpaque(false);
    }
}
