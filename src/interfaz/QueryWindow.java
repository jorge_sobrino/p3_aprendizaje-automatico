package interfaz;

import servicio.Data;
import servicio.Query;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class QueryWindow extends JFrame {

    private JLabel winner;

    Data data = new Data();

    public QueryWindow(){
        super("Aprendizaje automático: realizar consulta");
        setLayout(null);
        Font font = new Font("Agency FB", Font.BOLD, 35);

        JLabel tournament = new JLabel("Tournament:");
        tournament.setBounds(15,15,200,40);
        add(tournament);

        JTextField tournamentText = new JTextField(15);
        tournamentText.setBounds(100,15,300,50);
        add(tournamentText);

        JLabel p1_rank = new JLabel("Player 1 rank:");
        p1_rank.setBounds(15,75,200,40);
        add(p1_rank);

        JTextField p1_rankText = new JTextField(4);
        p1_rankText.setBounds(100,75,300,50);
        add(p1_rankText);

        JLabel p2_rank = new JLabel("Player 2 rank:");
        p2_rank.setBounds(15,135,200,40);
        add(p2_rank);

        JTextField p2_rankText = new JTextField(4);
        p2_rankText.setBounds(100,135,300,50);
        add(p2_rankText);

        JButton boton = new JButton("Hacer consulta");
        boton.setBounds(30,300,200,40);
        add(boton);
        boton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Query q =  new Query(
                        tournamentText.getText(), Integer.parseInt(p1_rankText.getText()), Integer.parseInt(p2_rankText.getText())
                );
                String s = data.aplicarModelo(q);
                if(s.isEmpty()){
                    winner.setText("Error al realizar consulta");
                }else winner.setText(s);
            }
        });

        JLabel w = new JLabel("El ganador es:");
        w.setBounds(15, 220, 180, 30);
        add(w);

        winner = new JLabel();
        winner.setBounds(100, 220, 180, 40);
        add(winner);

        QueryBackground qb = new QueryBackground();
        add(qb);
        setSize(500, 400);
        setVisible(true);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        data.aprenderModelo();
    }
}
