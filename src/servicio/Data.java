package servicio;

import weka.classifiers.Classifier;
import weka.classifiers.trees.RandomForest;
import weka.core.Instances;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Data {

    private Instances leerInstancias(String ficherArff) {
        try {
            Instances inst = new Instances(new BufferedReader(new FileReader(ficherArff)));
            inst.setClassIndex(inst.numAttributes() - 1);

            return inst;
        }
        catch (IOException ex) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void aprenderModelo() {
        try {
            Classifier cls = new RandomForest();

            Instances inst = leerInstancias("./training_data/atp_matches_2018.arff");
            cls.buildClassifier(inst);

            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("./models/objetoJ48Iris.model"));
            oos.writeObject(cls);
            oos.flush();
            oos.close();
        }
        catch (Exception ex) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /*public String aplicarModelo2(Query consulta) {
        try{
            String[] valoresAtributos = {"player1","player2"};
            FileManagement f = new FileManagement();
            f.copiarFile("./test_data/modeloTest.arff");
            f.copiarFileStr("./test_data/estructura.arff", consulta.toString());

            Classifier clasificador = (Classifier) weka.core.SerializationHelper.read("./models/objetoJ48Iris.model");
            Instances data = leerInstancias("./test_data/estrutura.arff");
            f.borrarFile("./test_data/estructura.arff");

            return valoresAtributos[(int) clasificador.classifyInstance(data.instance(0))];
        }
        catch (Exception ex) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al intentar leer el modelo";
        }
    }*/

    public String aplicarModelo(Query q) {
        try{
            String[] valoresAtributos = {"Player_1", "Player_2"};
            Classifier clasificador  = (Classifier) weka.core.SerializationHelper.read("./models/objetoJ48Iris.model");
            Instances data = leerInstancias("./test_data/estructura.arff");
            return valoresAtributos[(int) clasificador.classifyInstance(data.instance(0))];
        }catch (Exception ex) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al intentar leer el modelo";
        }
    }
}
