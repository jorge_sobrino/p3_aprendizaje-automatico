package servicio;

public class Query {

    private String tourney_name;
    private String winner;
    private String winner_hand;
    private double winner_age;
    private String loser_hand;
    private double loser_age;
    private int player1_rank;
    private int player2_rank;

    /*public Query(String tourney_name, String winner, String winner_hand, double winner_age, String loser_hand, double loser_age, int player1_rank, int player2_rank){
        this.tourney_name = tourney_name;
        this.winner = winner;
        this.winner_hand = winner_hand;
        this.winner_age = winner_age;
        this.loser_hand = loser_hand;
        this.loser_age = loser_age;
        this.player1_rank = player1_rank;
        this.player2_rank = player2_rank;
    }*/

    public Query(String tourney_name, int player1_rank, int player2_rank){
        this.tourney_name = tourney_name;
        this.player1_rank = player1_rank;
        this.player2_rank = player2_rank;
    }

    public Query(){}

    public void setTourney_name(String tourney_name) {
        this.tourney_name = tourney_name;
    }

    public String getTourney_name() {
        return tourney_name;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner_hand(String winner_hand) {
        this.winner_hand = winner_hand;
    }

    public String getWinner_hand() {
        return winner_hand;
    }

    public void setWinner_age(double winner_age) {
        this.winner_age = winner_age;
    }

    public double getWinner_age() {
        return winner_age;
    }

    public void setLoser_hand(String loser_hand) {
        this.loser_hand = loser_hand;
    }

    public String getLoser_hand(){
        return loser_hand;
    }

    public void setLoser_age(double loser_age) {
        this.loser_age = loser_age;
    }

    public double getLoser_age() {
        return loser_age;
    }

    public void setPlayer1_rank(int player1_rank) {
        this.player1_rank = player1_rank;
    }

    public int getPlayer1_rank() {
        return player1_rank;
    }

    public void setPlayer2_rank(int player2_rank) {
        this.player2_rank = player2_rank;
    }

    public int getPlayer2_rank() {
        return player2_rank;
    }

    @Override
    public String toString(){
        return getTourney_name() + "," + getWinner() + "," + getWinner_hand() + "," + getWinner_age() + "," + getLoser_hand() + "," + getLoser_age() + ",?" + "\n%\n%";
    }
}
