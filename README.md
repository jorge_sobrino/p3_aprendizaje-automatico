# README #

# Practica 3. Aprendizaje automatico

## Carlos Soler y Jorge Sobrino
  
La finalidad de esta practica es llevar a cabo el analisis de un archivo de datos de partidos de la ATP en el año 2018 y realizar pronósticos de los partidos. 
  
## Cuestiones

## 1. Datos analizados

- tourney_name: Lugar donde se ha jugado el torneo.
- winner_hand: Mano buena del jugador que ha ganado el partido.
- winner_age: Edad del jugador que ha ganado el partido.
- loser_hand: Mano buena del jugador que ha perdido el partido.
- loser_age: Edad del jugador que ha perdido el partido.
- player1_rank: Ranking de la ATP del jugador 1.
- player2_rank: Ranking de la Atp del jugafor 2.
- winner: Jugador que ha ganado el partido.

## 2. Algorimo utilizado

Tras haber probado los diferentes algoritmos proporcionados por la aplicación Weka, he ha concluido aplicar el algoritmo RandomForest. Se ha elegido este algoritmo en función de su ROC AREA.En las siguientes imagenes se muestra los algoritmos probados y sus resultados:

## RandomForest
![Scheme](images/randomForest.png)
## RandomTree
![Scheme](images/randomTree.png)
## NaiveBayes
![Scheme](images/naiveBayes.png)

## 3. Aplicación
